
var express = require('express');
var app = express();

app.get('/', function (req, res) {
  console.log("received a request from " + req);
  res.send('Hello World!');
});

app.use(express.static('src/html'));
app.use(express.static('node_modules/bootstrap/dist'));

app.listen(3000, function () {
    console.log('Listening on port 3000');
});