# stemnyc-samples

Sample code for use in STEM Kids NYC
[bit.ly link](http://bit.ly/2fqwRvC)

# Developer Workspace
[![Contribute](http://beta.codenvy.com/factory/resources/codenvy-contribute.svg)](http://beta.codenvy.com/f?id=r8et9w6vohmqvro8)

# Stack to use

FROM [codenvy/node](https://hub.docker.com/r/codenvy/node/)

# How to set-up
Configure git global properties if not done:

`git config --global user.name "your name here"`

`git config --global user.email "your email here"`

`git clone https://armando0518@bitbucket.org/armando0518/ar_stemnyc_samples.git`

`cd ar_stemnyc_samples`

`./setup.sh`

# How to run

| #       | Description           | Command  |
| :------------- |:-------------| :-----|
| 1      | Run | `node app/app.js` |

# How to tell which outside host/port you need to point to:

create a run command with the following contents:
`echo ${server.3000/tcp}`